Projet Avion

Pour s'inscrire, login entre 3 et 6 caractères inclus
Pour se connecter, il faut être inscrit

Les touches pour se déplacer sont les flèches du clavier
Les touches pour tirer : barre d'espace pour le laser, 'q' et 'f' pour les missiles en solo et en rafale

Pour la version multijoueurs, il faut démarrer le serveur avant et ouvrir le programme avion plusieurs fois en se connectant sous différents pseudos.

