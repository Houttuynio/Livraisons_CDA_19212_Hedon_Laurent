function validerChoix(){
    var inputs = document.getElementsByTagName("input");
    var selectedValue;
    for (const i of inputs) {
        if (i.checked){
            selectedValue = i.nextElementSibling.textContent;
        }
    }
    document.getElementById("resultat").setAttribute("value",selectedValue);
}