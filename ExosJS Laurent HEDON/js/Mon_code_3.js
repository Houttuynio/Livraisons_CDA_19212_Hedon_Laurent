function perimetre(){
    if (arguments.length===0)
        console.log("Périmètre impossible à calculer sans argument");
    else if (arguments.length===1)
        console.log("Périmètre du carré : "+ arguments[0]*4);
    else if (arguments.length===2)
        console.log("Périmètre du rectangle : " + 2*(parseInt(arguments[0])+parseInt(arguments[1])));
    else if (arguments.length===3)
        console.log("Périmètre du triangle : " + (parseInt(arguments[0])+parseInt(arguments[1])+parseInt(arguments[2])));
    else{
        var perimetre=0;
        for (const iterator of arguments) {
            perimetre=perimetre+parseInt(iterator);
        }
        console.log("Périmètre du polygone : " + perimetre);
    }
}

perimetre();
perimetre(2);
perimetre(2,3);
perimetre(3,4,5);
perimetre(5,6,7,8,9,10);

