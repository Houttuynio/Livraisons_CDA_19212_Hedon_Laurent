var startButton = document.getElementById('start');
var stopButton = document.getElementById('stop');
var pauseButton = document.getElementById('pause');
var chronoP = document.getElementById('chronoP');

startButton.paramTps = 0;
pauseButton.paramTps = 0;
var tpsEcoule = 0;
var decompte;

function startChrono(e) {
    var startTime = new Date();
    startButton.style.visibility = 'hidden';
    stopButton.style.visibility = 'visible';
    pauseButton.style.visibility = 'visible';
    startButton.removeEventListener("click", startChrono,false);
    stopButton.addEventListener("click", stopChrono,false);
    pauseButton.removeEventListener("click", startChrono,false);
    pauseButton.addEventListener("click", pauseChrono,false);
    decompte = setInterval(function () {
        // 1- Convertir en secondes :
        var seconds = Math.round((new Date().getTime() - startTime.getTime()) / 1000 + e.target.paramTps); // e représente l'event déclencheur
        // e.target représente l'objet déclencheur
        // ici : bouton start ou bouton pause
        // (cette prop a été ajoutée aux boutons)
        // 2- Extraire les heures:
        var hours = parseInt(seconds / 3600);
        seconds = seconds % 3600; // secondes restantes
        // 3- Extraire les minutes:
        var minutes = parseInt(seconds / 60);
        seconds = seconds % 60; // secondes restantes
        // 4- afficher dans le span
        chronoP.innerHTML = ajouteUnZero(hours)
            + ":" + ajouteUnZero(minutes)
            + ":" + ajouteUnZero(seconds);
        // 5- incrémenter le nombre de secondes
        console.log(chronoP.innerHTML);
        tpsEcoule += 1;
    }, 1000); // fin de function anonyme dans setInterval()
}


function ajouteUnZero(temps) {
    if (parseInt(temps) < 10) {
        return "0" + temps;
    }
    else {
        return temps;
    }
}

function stopChrono(){
    clearInterval(decompte);
    pauseButton.removeEventListener("click", pauseChrono,false);
    stopButton.removeEventListener("click", stopChrono,false);
    startButton.addEventListener("click", startChrono,false);
    pauseButton.paramTps=0;
    startButton.paramTps=0;
    tpsEcoule=0;
    chronoP.textContent="00:00:00";
    stopButton.style.visibility = 'hidden';
    pauseButton.style.visibility = 'hidden';
    startButton.style.visibility = 'visible';

}

function pauseChrono(){
    clearInterval(decompte);
    pauseButton.paramTps=tpsEcoule;
    pauseButton.removeEventListener("click", pauseChrono,false);
    pauseButton.addEventListener("click", startChrono,false);
}

startButton.addEventListener("click", startChrono,false);

