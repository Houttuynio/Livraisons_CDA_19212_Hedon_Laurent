Le serveur démarre en écoute sur le port 1515.
Les fichiers index.html, 404.html, 405.html, post.html, ainsi que les éventuels fichiers à tester pour les requêtes GET doivent se trouver dans le même répertoire que le jar.
Si besoin de changer le port ou le répertoire, c'est possible dans le fichier Constantes.java (dans ce cas, déplacer les fichiers cités précédemment dans le répertoire aussi).

